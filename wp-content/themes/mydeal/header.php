<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header class="header">
	<div class="header-top">
		<div class="container">
			<div class="header-top-content">
				<a href=""><i class="fa fa-plus-square-o" aria-hidden="true"></i>Đăng ký</a>
				<a href=""><i class="fa fa-user-circle-o" aria-hidden="true"></i>Đăng nhập</a>
				<a href=""><i class="fa fa-plus-square-o" aria-hidden="true"></i>Đăng ký email</a>
			</div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="container">
			<div class="logo">
			<!-- <a href="<?php echo get_option('home');?>"><img src="<?php echo esc_url( get_template_directory_uri() )?>/assets/images/img/logo.png"></a> -->
				<?php
                    if ( function_exists( 'the_custom_logo' ) ) {
					    the_custom_logo();
					}
	            ?>
			</div>
			<nav class="menu">
				<div class="main-menu">
					<?php
		                if(function_exists('wp_nav_menu')){
		                    $args = array(
		                        'theme_location' => 'primary',
		                        'link_before'=>'',
		                        'link_after'=>'',
		                        'container_class'=>'',
		                        'menu_class'=>'menu-primary',
		                        'menu_id'=>'',
		                        'container'=>'ul',
		                        'before'=>'',
		                        'after'=>''
		                    );
		                    wp_nav_menu( $args );
		                }
		            ?>
				</div>
				<div class="mobile-menu"></div>
			</nav>
		</div>
	</div>
	<div class="header-menu">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 header-menu-left">
					<span class="title-menu-product">Danh mục sản phẩm</span>
					<?php
		                // if(function_exists('wp_nav_menu')){
		                //     $args = array(
		                //         'theme_location' => 'product',
		                //         'link_before'=>'',
		                //         'link_after'=>'',
		                //         'container_class'=>'',
		                //         'menu_class'=>'menu-product',
		                //         'menu_id'=>'',
		                //         'container'=>'ul',
		                //         'before'=>'',
		                //         'after'=>''
		                //     );
		                //     wp_nav_menu( $args );
		                // }
		            ?>

<?php
	$taxonomy_name = 'product_cat';
	$terms = get_terms('product_cat', array(
	    'parent'=> 0,
	    'hide_empty' => false
	) );

	echo '<ul id="menu-menu-san-pham" class="menu-product">';

		$query = tiep_cus_post_archive_query_paged('product', 1000);

		$countsptax = 0;
		if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
			$countsptax++;
		endwhile; wp_reset_postdata(); endif;

		?>

			<li>
				<a href="<?php echo get_option('home'); ?>" title="">
					<i class="_mi _before fa fa-list" aria-hidden="true"></i>
					<span><?php echo 'Tất cả các deal'; echo ' ('.$countsptax.') '; ?></span>
				</a>
			</li>

		<?php

		foreach($terms as $term){
			$team_lg = $term->term_id;
			
			$query = tiep_custom_posttype_query('product', $taxonomy_name, $term->term_id, 1000);

			$countsptax = 0;
			if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
				$countsptax++;
			endwhile; wp_reset_postdata(); endif;

			?>

	    	<li>
	    		<a href="<?php echo get_term_link(get_term($term->term_id)); ?>" title="">
	    			<?php echo types_render_termmeta( "icon-mo-ta-danh-muc", array('term_id' => $term->term_id ) );?>
	    			<span><?php echo $term->name; echo ' ('.$countsptax.') '; ?></span>
	    		</a>
	    		<?php
					$thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
					$image = wp_get_attachment_url( $thumbnail_id );
					// echo '<img src="'.$image.'" alt="" width="50" height="50" />';
	    		?>

				<?php
					$term_childs = get_term_children( $term->term_id, $taxonomy_name );
					$count = count($term_childs);
					if($count > 0) {

						echo '<ul class="sub-menu" style="background:rgb(255, 255, 255) url('.$image.') no-repeat scroll right bottom; ">';
						
						?>

							<li>
					    		<a href="javascript:void(0)" title="">
					    			<span><?php echo get_cat_name($term->term_id); ?></span>
					    		</a>
					    	</li>

						<?php

						foreach ( $term_childs as $child ) {
						    $term = get_term_by( 'id', $child, $taxonomy_name );
						    if($term->parent == $team_lg){

								$query = tiep_custom_posttype_query('product', $taxonomy_name, $term->term_id, 1000);

								$countsptax = 0;
								if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
									$countsptax++;
								endwhile; wp_reset_postdata(); endif;

						    	?>
						    	    <li>
							    		<a href="<?php echo get_term_link(get_term($term->term_id)); ?>" title="">
							    			<span><?php echo $term->name; echo ' ('.$countsptax.') '; ?></span>
							    		</a>
							    	</li>
						    	<?php
						    }
						}

						echo '</ul>';

					}
				?>
	    	</li>

	    	<?php
		}
	echo '</ul>';
?>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 header-menu-right">
					<div class="search-box">
						<form action="<?php echo esc_url( home_url( '/' ) ); ?>">
		                    <input type="text" placeholder="<?php _e('Nhập số điện thoại để tra cứu đơn hàng', 'mydeal'); ?>" name="s" value="<?php echo get_search_query(); ?>">
		                    <button type="submit" class="search-icon">
		                    	<?php _e('Tìm kiếm', 'mydeal'); ?>
		                    </button>
		                </form>
					</div>
					<a class="cart-box" href="<?php echo wc_get_cart_url();?>">
						<span class="cart-cart"><i class="fa fa-cart-plus" aria-hidden="true"></i></span>
						<span class="cart-quantity"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
						<span class="cart-total">
							<?php
								$a = WC()->cart->cart_contents_total;
								echo tiep_format_price($a, get_woocommerce_currency_symbol() );
								// var_dump(WC());
							?>
						</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</header>




