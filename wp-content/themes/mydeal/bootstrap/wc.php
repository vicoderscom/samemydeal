<?php
//thay đổi nút mua hàng page single product
function tiep_return_button_add_to_cart( $output ) {
        $output = 'Mua ngay';
        return $output;
    }
add_filter( 'woocommerce_product_single_add_to_cart_text', 'tiep_return_button_add_to_cart' );

//thay đổi nút đặt hàng page checkout
function tiep_return_text_check_out( $output ) {
        $output = 'Đặt hàng';
        return $output;
    }
add_filter( 'woocommerce_order_button_text', 'tiep_return_text_check_out' );



//thay đổi nút mua hàng ajax (chưa cần)
function tiep_chance_loop_add_to_cart_link( $output ) {
    global $product;
    $test = sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
        esc_url( $product->add_to_cart_url() ),
        esc_attr( isset( $quantity ) ? $quantity : 1 ),
        esc_attr( $product->get_id() ),
        esc_attr( $product->get_sku() ),
        esc_attr( isset( $class ) ? $class : 'button' ),
        'Mua hàng'
    );
    return $test;
}
add_filter( 'woocommerce_loop_add_to_cart_link', 'tiep_chance_loop_add_to_cart_link' );



//sửa đơn vị giá mặc định của wc
add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);
function change_existing_currency_symbol( $currency_symbol, $currency ) {
    switch( $currency ) {
        case 'VND': $currency_symbol = '<sup>đ</sup>'; break;
    }
    return $currency_symbol;
}

//thay đổi mũi tên trên breadcrumb của wc
add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
function jk_woocommerce_breadcrumbs() {
    return array(
            'delimiter'   => ' > ',
            'wrap_before' => '<nav class="woocommerce-breadcrumb" itemprop="breadcrumb">',
            'wrap_after'  => '</nav>',
            'before'      => '',
            'after'       => '',
            'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
        );
}

//show số lượng sản phẩm đã được bán ra
function show_count_price_product() {
    global $post;
    $count = get_post_meta($post->ID,'total_sales', true);
    $result_count = sprintf( _n( '%s', '%s', $count, 'wpdocs_textdomain' ), number_format_i18n($count));
    return $result_count;
}

//tuy chinh checkout
function customCheckoutFields($fields) {
    // echo '<pre>';
    // var_dump($fields);
    // echo '</pre>';

    unset($fields['billing']['billing_company']);

    $fields['billing']['billing_country']['required'] = false;
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_postcode']);

    $fields['billing']['billing_city']['required'] = false;
    unset($fields['billing']['billing_last_name']);

    $fields['billing']['billing_first_name']['label'] = __('Họ tên');
    $fields['billing']['billing_first_name']['class'] = ['form-row-wide'];

    $fields['billing']['billing_address_1']['label'] = __('Địa chỉ');
    $fields['billing']['billing_address_1']['class'] = ['form-row-wide'];
    $fields['billing']['billing_address_1']['placeholder'] = '';

    $fields['billing']['billing_phone']['label'] = __('Số điện thoại');
    $fields['billing']['billing_phone']['class'] = ['form-row-wide'];

    $fields['billing']['billing_email']['label'] = __('Thư điện tử');
    $fields['billing']['billing_email']['class'] = ['form-row-wide'];

    unset($fields['account']['account_password']);
    
    $fields['order']['order_comments']['label'] = __('Nội dung');
    $fields['order']['order_comments']['class'] = ['form-row-wide'];
    $fields['order']['order_comments']['placeholder'] = '';

    //them field
    // $fields['billing']['billing_message'] = array(
    //     'type'      => 'textarea',
    //     'label'     => __('Nội dung', 'woocommerce'),
    //     'placeholder'   => _x('', 'placeholder', 'woocommerce'),
    //     'class'     => array('form-row-wide'),
    //     'required'  => false,
    //     'clear'     => true,
    //     'label_class' => ''
    // );

    // echo '<pre>';
    // var_dump($fields);
    // echo '</pre>';
    
    return $fields;
}
add_filter('woocommerce_checkout_fields' , 'customCheckoutFields');


//do bi ham nao do de len nen khong fix dc require nen anh duy lam the
add_filter('woocommerce_get_country_locale_default', 'testfield');

function testfield($data)
{
    $data['city']['label'] = 'Thanh pho';
    $data['city']['required'] = false;
    // echo '<pre>';
    // var_dump($data);
    // echo '</pre>';

    return $data;
}
