<?php
//custom logo
class setting {
    function __construct(){
        add_theme_support( 'custom-logo' );
    }
}
new setting();

//lay img
function tiep_get_thumbnail_url( $size = 'full' ) {
    global $post;
    if (has_post_thumbnail( $post->ID ) ) {
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $size );
        return $image[0];
    }

    // use first attached image
    $images = get_children( 'post_type=attachment&post_mime_type=image&post_parent=' . $post->ID );
    if (!empty($images)) {
        $image = reset($images);
        $image_data = wp_get_attachment_image_src( $image->ID, $size );
        return $image_data[0];
    }

    // use no preview fallback
    if ( file_exists( get_template_directory().'/assets/images/img/logo.png' ) )
        return get_template_directory_uri().'/assets/images/img/logo.png';
    else
        return get_template_directory_uri().'/assets/images/img/logo.png';
}

//cat chuoi
function cut_string($str,$len,$more){
    if ($str=="" || $str==NULL) return $str;
    if (is_array($str)) return $str;
        $str = trim(strip_tags($str));
    if (strlen($str) <= $len) return $str;
        $str = substr($str,0,$len);
    if ($str != "") {
        if (!substr_count($str," ")) {
          if ($more) $str .= " ...";
          return $str;
        }
        while(strlen($str) && ($str[strlen($str)-1] != " ")) {
            $str = substr($str,0,-1);
        }
        $str = substr($str,0,-1);
        if ($more) $str .= " ...";
    }
    return $str;
}

//lay view
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

//seach
function filter_search($query) {
    if ($query->is_search) {
        $query->set('post_type', array('product'));
    };
    return $query;
};//bo post di de show product con co gia
add_filter('pre_get_posts', 'filter_search');

//check gia
function tiep_format_price($money, $cur){
    $str = "";
    if($money != 0){
        $num = (float)$money;
        $str = number_format($num,0,'.','.');
        $str .= $cur;
        $str = $str;
    }
    return $str;
}
function tiep_get_price($old_price, $price, $donvi2){
    $donvi = !empty($donvi2) ? $donvi2 : '';
    $str = "";
    if($old_price > 0){
        if($price > 0 || $price != null){
            $str1 = tiep_format_price($price, $donvi);
            $str2 = tiep_format_price($old_price, $donvi);
            $str = '<div class="price"><span class="price-news">'.$str1.'</span><span class="price-old">'.$str2.'</span></div>';
        } else {
            $str = '<div class="price"><span class="price-one">'.tiep_format_price($old_price, $donvi).'</span></div>';
        }
    } else{
        $str = '<a class="contactsky" href="">Đặt hàng</a>';
    }
    return $str;
}

//query sp cua category
function tiep_custom_posttype_query($posttype, $taxonomy, $termId, $numPost){
	$qr =  new WP_Query( array(
			            'post_type' => $posttype,
			            'tax_query' => array(
			                                array(
			                                        'taxonomy' => $taxonomy,
			                                        'field' => 'id',
			                                        'terms' => $termId,
			                                        'operator'=> 'IN'
			                                 )),
			            'showposts'=>$numPost,
			            'order' => 'DESC',
			            'orderby' => 'date'
			     ) );
	return $qr;
}
//query tat ca sp
function tiep_cus_post_archive_query_paged($posttype, $size){
        $qr =  new WP_Query( array(
                        'post_type' => $posttype,
                        'posts_per_page' => $size,
                        'order' => 'DESC',
                        'orderby' => 'date'
                 ) );
    return $qr;
}

