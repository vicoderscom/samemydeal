<?php get_header(); ?>
<section class="main-content">
	<div class="container">
		<div class="row">
			<main class="page-content">
				<style type="text/css">
				    .page-404 {
				        min-width: 50%;
				        padding: 0;
				        margin: auto;
				        text-align: center;
				    }

				    .page-404-text-err {
				        font-size: 80px;
				        text-align: center;
				        font-weight: bold;
				        display:block;
				    }
				    .page-404-title{
				    	margin: 20px 0 0;
				    	font-size: 2em;
				    }
				</style>
				<div class="page-404">
					<img alt="<?php echo get_bloginfo("name"); ?>" src="<?php echo esc_url( get_template_directory_uri() )?>/assets/images/img/after-logo.png" />
			        <span class="page-404-text-err">404</span>
			        <h1 class="page-404-title">We're sorry...</h1>
			        <p>The page or journal you are looking for cannot be found.</p>
			        <p><a href="#">Return to the homepage</a></p>
				</div>
			</main>
		</div>
	</div>
</section>
<?php get_footer(); ?>