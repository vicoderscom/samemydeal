<?php get_header(); ?>
<section class="page-content">
	<div class="container">
		<div class="row">
			<?php get_sidebar("news");?>
			<main class="col-lg-9 col-md-9 col-sm-9 col-xs-12 page-article">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article class="detail-page">
						<div class="title-page"><h1><?php the_title();?></h1></div>
						<div class="description-page">
							<?php the_content();?>
						</div>
					</article>
				<?php endwhile; endif; wp_reset_query(); ?>
			</main>
		</div>
	</div>
</section>
<?php get_footer(); ?>