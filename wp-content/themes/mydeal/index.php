<?php get_header(); ?>
<section class="banner-home">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 pull-right shadow banner-content">
			<?php
				$query = new WP_Query( array(
			        'post_type' => array( 'banner-trang-chu' ),
			        'post_status' => array( 'publish' ),
			        'showposts' => 100,
			        'order' => 'DESC',
			        'orderby' => 'date'
				) );
				if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post(); ?>
					<a href="<?php echo types_render_field( "url-banner", array("output"=>"raw") );?>">
						<img class="img-responsive" src="<?php echo tiep_get_thumbnail_url('full') ?>" />
					</a>
			<?php endwhile; wp_reset_query(); else: echo '<div class="update-loading">Đang cập nhật!</div>'; endif; ?>
			</div>
		</div>
	</div>
</section>

<div class="archive-product">
	<div class="container">
		<div class="archive-product-content">
			<div class="row">
				<?php get_template_part("resources/template/template-archive-product"); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>

