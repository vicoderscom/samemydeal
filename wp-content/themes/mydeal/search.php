<?php get_header(); ?>

<section class="page-content search">
	<div class="container">
		<div class="row">
			<?php get_sidebar("news");?>
			<main class="col-lg-9 col-md-9 col-sm-9 col-xs-12 page-article">

				<div class="woocommerce custom-breadcrumb-wc">
					<?php
						/**
						 * woocommerce_before_main_content hook.
						 *
						 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
						 * @hooked woocommerce_breadcrumb - 20
						 * @hooked WC_Structured_Data::generate_website_data() - 30
						 */
						do_action( 'woocommerce_before_main_content' );
					?>
				</div>

				<div class="row">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			        	<article class="col-lg-4 col-md-6 col-sm-6 col-xs-6 item">
			        		<div class="shadow p-info">
								<figure>
									<a href="<?php the_permalink();?>">
										<img class="img-responsive" src="<?php echo tiep_get_thumbnail_url('product') ?>" alt="<?php the_title();?>" />
									</a>
								</figure>

								<div class="p-detail">
								
									<div class="p-title">
										<a href="<?php the_permalink();?>">
											<h3>
												<?php the_title();?>									
											</h3>
										</a>
									</div>
									
			<?php get_template_part("resources/template/template-show-price"); ?>

									<a class="read-more" href="<?php the_permalink();?>">
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</a>
									
									<div class="p-view">
										<span class="p-view-count"><?php echo show_count_price_product(); ?></span>
										<i class="fa fa-user" aria-hidden="true"></i>
									</div>

								</div>

							</div>
						</article>
					<?php endwhile; endif; wp_reset_query(); ?>
				</div>
			</main>
		</div>
	</div>
</section>
<?php get_footer(); ?>
