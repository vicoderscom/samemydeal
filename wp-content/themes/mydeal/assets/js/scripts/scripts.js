$(document).ready(function(){

  
    if ($('.banner-content img').length > 1) {
    	$('.banner-content').slick({
    		    infinite: false,
            speed: 1000,
            slidesToShow: 1,
            slidesToScroll: 1,
            pauseOnHover: false,
            autoplay: true,
            autoplaySpeed: 2000,
            lazyLoad: 'ondemand',
            fade: false, //chi chay slide 1
            cssEase: '',
            dots: true,
            arrows: false,
            prevArrow: '<span class="slick-prev"></span>',
            nextArrow: '<span class="slick-next"></span>',
    	});
    }

    // if ($('.woocommerce-product-gallery__wrapper .woocommerce-product-gallery__image').length > 4) {
    //   $('.woocommerce-product-gallery__wrapper .slick-gallery').slick({
    //         infinite: false,
    //         speed: 1000,
    //         slidesToShow: 4,
    //         slidesToScroll: 1,
    //         pauseOnHover: false,
    //         autoplay: true,
    //         autoplaySpeed: 2000,
    //         lazyLoad: 'ondemand',
    //         fade: false,
    //         cssEase: '',
    //         dots: false,
    //         arrows: false,
    //         prevArrow: '<span class="slick-prev"></span>',
    //         nextArrow: '<span class="slick-next"></span>',
    //   });
    // }

    // if($('.venobox').length){
    //   $('.venobox').venobox({
    //       border: '10px', //viền xung quanh ảnh
    //       bgcolor: '#ffffff', //màu nền viền
    //       infinigall: true, //co lặp slide
    //   });
    // }

    
    //slide mydeal
    var count = $('.carousel-images .woocommerce-product-gallery__image').length;
    // alert(count);
    if(count < 6) {
      $('.carousel-images').addClass('transform3dslide');
      console.log("tiep");
    }

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: false,
        arrows: false,
        dots: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        pauseOnHover: false,
        speed: 1000,
        autoplaySpeed: 1000,
        asNavFor: '.slider-for',
        arrows: false,
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        autoplay: true,
    });


    if ($('.main-menu').length) {
        $('.main-menu').meanmenu({
            meanScreenWidth: "992",
            meanMenuContainer: ".mobile-menu",
        });
    }

    //back to top
    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 20) {
            $("#back-to-top").css("display", "block");
        } else {
            $("#back-to-top").css("display", "none");
        }
    });
    if($('#back-to-top').length){
        $("#back-to-top").on('click', function() {
           $('html, body').animate({
               scrollTop: $('html, body').offset().top
             }, 1000);
        });
    }

    //lay chieu cao menu
    // var height = $('.menu-product');
    // var $content = $('.sub-menu');
    // var height2 = height.height();
    // $content.height(height2);


    //facebook comment
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1606057776088685',
          xfbml      : true,
          version    : 'v2.9'
        });
        FB.AppEvents.logPageView();
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
});
