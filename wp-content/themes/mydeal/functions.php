<?php
//include widget
include_once get_template_directory(). '/src/Widgets/ads.php';
include_once get_template_directory(). '/src/Widgets/social-footer.php';
include_once get_template_directory(). '/src/Widgets/support.php';

include_once get_template_directory(). '/bootstrap/app.php';
include_once get_template_directory(). '/bootstrap/wc.php';



$vendor_autoload_file = __DIR__ . '/../vendor/autoload.php';
if (file_exists($vendor_autoload_file)) {
    require $vendor_autoload_file;
}

define('WOOCOMMERCE_USE_CSS', true);
/**
 * Enqueue scripts and stylesheet
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
add_action('wp_enqueue_scripts', 'theme_enqueue_style');

function theme_enqueue_style()
{
    wp_enqueue_style('template-style', get_stylesheet_directory_uri() . '/dist/css/app.all.css', false);
}

function theme_enqueue_scripts()
{
    wp_enqueue_script('template-scripts', get_stylesheet_directory_uri() . '/dist/js/scripts.js', 'jquery');
    wp_enqueue_script('template-browserify', get_stylesheet_directory_uri() . '/dist/js/app.js', 'template-scripts');

    $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
    $params = array(
        'ajaxurl' => admin_url('admin-ajax.php', $protocol)
    );
    wp_localize_script('template-scripts', 'ajax_obj', $params); 
}

/**
 * helpers
 */
function asset($path)
{
    return substr($path, 0, 1) == '/' ? get_stylesheet_directory_uri() . '/assets' . $path : get_stylesheet_directory_uri() . '/assets/' . $path;
}




if (!function_exists('mydealSetup')) {
    /**
     * setup support for theme
     *
     * @return void
     */
    function mydealSetup()
    {
        // Register menus
        register_nav_menus( array(
            'primary' => __( 'Menu chính', 'mydeal' ),
            'product'  => __( 'Menu sản phẩm', 'mydeal' ),
            'about'  => __( 'Menu giới thiệu', 'mydeal' ),
            'contact'  => __( 'Menu trợ giúp', 'mydeal' ),
            'partner'  => __( 'Menu hợp tác', 'mydeal' ),
            'taxonomy-footer'  => __( 'Menu danh mục chân trang', 'mydeal' ),
        ) );

        // add_theme_support('custom-logo');
        // add_theme_support('menus');
        add_theme_support('post-thumbnails');
        add_image_size('product', 360, 315, true);
    }

    add_action('after_setup_theme', 'mydealSetup');
}


//xoa class va id wp_nav_menu()
function wp_nav_menu_attributes_filter($var) {
    return is_array($var) ? array_intersect($var, array('current-menu-item','mega-menu')) : '';
}
add_filter('nav_menu_css_class', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('page_css_class', 'wp_nav_menu_attributes_filter', 100, 1);


//dang ky sidebar
if (!function_exists('mydealWidgets')) {
    /**
     * register sidebar for theme
     *
     * @return void
     */
    function mydealWidgets()
    {
        $sidebars = [
            [
                'name'          => __('Footer Column1', 'mydeal'),
                'id'            => 'footer-1',
                'description'   => __('This is sidebar for footer column1', 'mydeal'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Footer Column2', 'mydeal'),
                'id'            => 'footer-2',
                'description'   => __('This is sidebar for footer column2', 'mydeal'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Footer Column3', 'mydeal'),
                'id'            => 'footer-3',
                'description'   => __('This is sidebar for footer column3', 'mydeal'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Footer Column4', 'mydeal'),
                'id'            => 'footer-4',
                'description'   => __('This is sidebar for footer column4', 'mydeal'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Footer Copyright', 'mydeal'),
                'id'            => 'footer-copyright',
                'description'   => __('This is sidebar for footer copyright', 'mydeal'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Footer Social', 'mydeal'),
                'id'            => 'footer-social',
                'description'   => __('This is sidebar for footer social', 'mydeal'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Footer Taxonomy', 'mydeal'),
                'id'            => 'footer-taxonomy',
                'description'   => __('This is sidebar for footer taxonomy', 'mydeal'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('ads', 'mydeal'),
                'id'            => 'ads',
                'description'   => __('This is sidebar for ads', 'mydeal'),
                'before_widget' => '<section id="%1$s" class="widget %2$s hidden-xs">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('support', 'mydeal'),
                'id'            => 'support',
                'description'   => __('This is sidebar for support', 'mydeal'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }
    add_action('widgets_init', 'mydealWidgets');
}

/**
 * Make shortcode run in widget
 *
 */
add_filter('widget_text', 'do_shortcode');








?>