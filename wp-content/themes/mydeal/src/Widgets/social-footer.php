<?php
class social extends WP_Widget {
    function __construct() {
        parent::__construct(
            'social',
            'social',
            array( 'description'  =>  'Tiệp - social' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => '',
            'facebook' => '',
            'twitter' => '',
            'google' => '',
            'youtube' => '',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $facebook = esc_attr($instance['facebook']);
        $twitter = esc_attr($instance['twitter']);
        $google = esc_attr($instance['google']);
        $youtube = esc_attr($instance['youtube']);

        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo '<p>Link facebook:<input type="text" class="widefat" name="'.$this->get_field_name('facebook').'" value="'.$facebook.'" /></p>';
        echo '<p>Link twitter:<input type="text" class="widefat" name="'.$this->get_field_name('twitter').'" value="'.$twitter.'" /></p>';
        echo '<p>Link google:<input type="text" class="widefat" name="'.$this->get_field_name('google').'" value="'.$google.'" /></p>';
        echo '<p>Link youtube:<input type="text" class="widefat" name="'.$this->get_field_name('youtube').'" value="'.$youtube.'" /></p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['facebook'] = ($new_instance['facebook']);
        $instance['twitter'] = ($new_instance['twitter']);
        $instance['google'] = ($new_instance['google']);
        $instance['youtube'] = ($new_instance['youtube']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $facebook = $instance['facebook'];
        $twitter = $instance['twitter'];
        $google = $instance['google'];
        $youtube = $instance['youtube'];

        echo $before_widget;
            echo '<a href="'.$facebook.'"><i class="fa fa-facebook" aria-hidden="true"></i></a>';
            echo '<a href="'.$twitter.'"><i class="fa fa-twitter" aria-hidden="true"></i></i></a>';
            echo '<a href="'.$google.'"><i class="fa fa-google-plus" aria-hidden="true"></i></a>';
            echo '<a href="'.$youtube.'"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>';
        echo $after_widget;
    }
}
function create_social_widget() {
    register_widget('social');
}
add_action( 'widgets_init', 'create_social_widget' );
?>