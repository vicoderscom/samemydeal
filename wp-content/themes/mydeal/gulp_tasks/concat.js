var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require("gulp-concat");
module.exports = [
    ['sass'],
    function() {
        gulp.src([
                './bower_components/bootstrap/dist/css/bootstrap.min.css',
                './bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
                './bower_components/font-awesome/css/font-awesome.min.css',
                './bower_components/jquery.meanmenu/meanmenu.min.css',
                './bower_components/slick-carousel/slick/slick.css',
                // './bower_components/venobox/venobox/venobox.css',
                './dist/css/app.css'
            ])
            .pipe(concat("app.all.css"))
            .pipe(gulp.dest('./dist/css/'));
    }
];
