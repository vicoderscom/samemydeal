var gulp = require('gulp');
var concat = require('gulp-concat');	//Để nối nội dung các file
module.exports = function() {
    return gulp.src([
    		'./bower_components/jquery/dist/jquery.min.js',
            './bower_components/bootstrap/dist/js/bootstrap.min.js',
            './bower_components/jquery.meanmenu/jquery.meanmenu.min.js',
            './bower_components/slick-carousel/slick/slick.min.js',
            // './bower_components/venobox/venobox/venobox.min.js',
            './assets/js/scripts/*.js',
        ])
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('./dist/js/'));
};
