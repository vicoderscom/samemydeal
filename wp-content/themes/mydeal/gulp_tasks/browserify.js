var gulp = require('gulp');	//tìm kiếm trong node_modules package có tên gulp, gán nội dung tới biến gulp
var browserify = require('gulp-browserify');
module.exports = function() {
    return gulp.src('./assets/js/browserify/app.js') // Lấy các tệp nguồn với gulp.src
        .pipe(browserify({
            transform: ['babelify'],
        }))								// Gửi nó qua một plugin gulp
        .pipe(gulp.dest('./dist/js'));	// Xuất file kết quả trong thư mục này
}
