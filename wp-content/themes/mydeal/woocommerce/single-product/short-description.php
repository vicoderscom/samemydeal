<?php
/**
 * Single product short description
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/short-description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

if ( ! $post->post_excerpt ) {
	return;
}

?>

<div class="woocommerce-product-details__short-description">
    <?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ); ?>
</div>
<ul class="service-info-single">
    <li><i class="fa fa-car" aria-hidden="true"></i><span>Giao sản phẩm</span>toàn quốc</li>
    <li><i class="fa fa-user" aria-hidden="true"></i><span><?php echo show_count_price_product(); ?></span>đã mua</li>
    <li><i class="fa fa-mobile" aria-hidden="true"></i>Hỗ trợ đặt hàng<span>1900 6420</span></li>
</ul>

<div class="single_variation_wrap_no_price">
	<div class="price-single">
		<?php get_template_part("resources/template/template-show-price"); ?>
	</div>
</div>

