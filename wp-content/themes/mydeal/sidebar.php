<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 sidebar">
	<aside class="sidebar-content">
		<div class="title-sidebar">Giới thiệu</div>
        <?php
            if(function_exists('wp_nav_menu')){
                $args = array(
                    'theme_location' => 'about',
                    'link_before'=>'',
                    'link_after'=>'',
                    'container_class'=>'',
                    'menu_class'=>'menu-about',
                    'menu_id'=>'',
                    'container'=>'ul',
                    'before'=>'',
                    'after'=>''
                );
                wp_nav_menu( $args );
            }
        ?>
        <div class="title-sidebar">Trợ giúp</div>
        <?php
            if(function_exists('wp_nav_menu')){
                $args = array(
                    'theme_location' => 'contact',
                    'link_before'=>'',
                    'link_after'=>'',
                    'container_class'=>'',
                    'menu_class'=>'menu-contact',
                    'menu_id'=>'',
                    'container'=>'ul',
                    'before'=>'',
                    'after'=>''
                );
                wp_nav_menu( $args );
            }
        ?>
        <div class="title-sidebar">Hợp tác</div>
		<?php
            if(function_exists('wp_nav_menu')){
                $args = array(
                    'theme_location' => 'partner',
                    'link_before'=>'',
                    'link_after'=>'',
                    'container_class'=>'',
                    'menu_class'=>'menu-partner',
                    'menu_id'=>'',
                    'container'=>'ul',
                    'before'=>'',
                    'after'=>''
                );
                wp_nav_menu( $args );
            }
        ?>
	</aside>
</div>