<?php
	$check_id = Get_queried_object();
	// var_dump($check_id);
	if (!empty($check_id)) {
		$check_yes = $check_id->term_id;
	}
	if (empty($check_yes)) {
		$queryda2 =  tiep_cus_post_archive_query_paged("product", 1000); $i=0;
        if($queryda2->have_posts()) : while ($queryda2->have_posts() ) : $queryda2->the_post(); ?>
        <?php
        	if($i==9) {
        		dynamic_sidebar( 'ads' );
        	}
        ?>
        	<article class="col-lg-4 col-md-4 col-sm-4 col-xs-6 item">
        		<div class="shadow p-info">
					<figure>
						<a href="<?php the_permalink();?>">
							<img class="img-responsive" src="<?php echo tiep_get_thumbnail_url('product') ?>" alt="<?php the_title();?>" />
						</a>
					</figure>

					<div class="p-detail">
					
						<div class="p-title">
							<a href="<?php the_permalink();?>">
								<h3>
									<?php the_title();?>									
								</h3>
							</a>
						</div>
						
<?php get_template_part("resources/template/template-show-price"); ?>

						<a class="read-more" href="<?php the_permalink();?>">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</a>
						
						<div class="p-view">
							<span class="p-view-count"><?php echo show_count_price_product(); ?></span>
							<i class="fa fa-user" aria-hidden="true"></i>
						</div>

					</div>

				</div>
			</article>
        <?php $i++; endwhile; wp_reset_postdata(); else: echo ''; endif;

	} else {
		
		$query = tiep_custom_posttype_query('product', 'product_cat', $check_yes, 300); $i=0;
		if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post(); ?>
		<?php
        	if($i==9) {
        		dynamic_sidebar( 'ads' );
        	}
        ?>
        	<article class="col-lg-4 col-md-4 col-sm-4 col-xs-6 item">
        		<div class="shadow p-info">
					<figure>
						<a href="<?php the_permalink();?>">
							<img class="img-responsive" src="<?php echo tiep_get_thumbnail_url('product') ?>" alt="<?php the_title();?>" />
						</a>
					</figure>
					
					<div class="p-detail">

						<div class="p-title">
							<a href="<?php the_permalink();?>">
								<h3>
									<?php the_title();?>									
								</h3>
							</a>
						</div>
						
						<?php get_template_part("resources/template/template-show-price"); ?>

						<a class="read-more" href="<?php the_permalink();?>">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</a>

						<div class="p-view">
							<span class="p-view-count"><?php echo show_count_price_product(); ?></span>
							<i class="fa fa-user" aria-hidden="true"></i>
						</div>

					</div>

				</div>
			</article>
		<?php $i++; endwhile; wp_reset_postdata(); else: echo ''; endif;
	}
?>